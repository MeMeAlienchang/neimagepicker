# NEImagePicker

[![CI Status](https://img.shields.io/travis/1217493217@qq.com/NEImagePicker.svg?style=flat)](https://travis-ci.org/1217493217@qq.com/NEImagePicker)
[![Version](https://img.shields.io/cocoapods/v/NEImagePicker.svg?style=flat)](https://cocoapods.org/pods/NEImagePicker)
[![License](https://img.shields.io/cocoapods/l/NEImagePicker.svg?style=flat)](https://cocoapods.org/pods/NEImagePicker)
[![Platform](https://img.shields.io/cocoapods/p/NEImagePicker.svg?style=flat)](https://cocoapods.org/pods/NEImagePicker)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NEImagePicker is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NEImagePicker'
```

## Author

1217493217@qq.com, chang.liu.meme@funpuls.com

## License

NEImagePicker is available under the MIT license. See the LICENSE file for more info.
